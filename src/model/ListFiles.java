package model;

import java.io.File;

/**
 * Класс, представляющий модель каталога файлов
 *
 * @authors СмЕшНяФфФкиИ
 */
public class ListFiles {
    /**
     * Путь к каталогу
     */
    private File file;

    /**
     * Конструктор
     *
     * @param file - путь к каталогу
     */
    public ListFiles(File file) {
        this.file = file;
    }

    /**
     * Возвращает путь к каталогу
     *
     * @return путь к каталогу
     */
    public File getFile() {
        return file;
    }
}