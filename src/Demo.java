import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Класс, запускающий приложение
 *
 * @authors СмЕшНяФфФкиИ
 */
public class Demo extends Application {
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("view/MainWindow.fxml"));
        primaryStage.setTitle("FailMan");
        primaryStage.setScene(new Scene(root, 600, 450));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("img/icon.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}