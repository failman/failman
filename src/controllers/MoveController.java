package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Класс-контроллер для перемещения файла
 *
 * @authors СмЕшНяФфФкиИ
 */
public class MoveController {
    /**
     * Кнопка для запуска перемещения файла
     */
    @FXML
    private Button ok;
    /**
     * Путь, по которому нужно переместить указанный файл
     */
    @FXML
    private TextField pathTo;

    public void initialize() {
        /*
         * Реализация кнопки запуска перемещения файла
         */
        ok.setOnAction(event -> {
            String to = pathTo.getText();
            try {
                Files.move(Paths.get(MainController.getPathForCopyAndMoving()), Paths.get(to + MainController.getNameForCopyAndMoving()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage stage = (Stage) ok.getScene().getWindow();
            stage.close();
        });
    }
}
