package controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.ListFiles;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Класс-контроллер, отвечающий за главное окно и вызов вспомогательных
 *
 * @authors СмЕшНяФфФкиИ
 */
public class MainController {
    private ObservableList<ListFiles> usersData = FXCollections.observableArrayList();
    /**
     * Таблица, отображающая содержимое заданного каталога
     */
    @FXML
    private TableView<ListFiles> files;
    /**
     * Колонка таблицы, отвечающая за имя файла
     */
    @FXML
    private TableColumn<File, String> nameFile;
    /**
     * Поле ввода для имени файла, с которым пользователь хочет работать
     */
    @FXML
    private TextField fileField;
    /**
     * Кнопка перехода в новый каталог
     */
    @FXML
    private FontAwesomeIconView goTo;
    /**
     * Поле ввода для пути к каталогу, с которым пользователь хочет работать
     */
    @FXML
    private TextField pathField;
    /**
     * Кнопка для архивирования
     */
    @FXML
    private FontAwesomeIconView archive;
    /**
     * Кнопка для разархивирования
     */
    @FXML
    private FontAwesomeIconView unarchive;
    /**
     * Кнопка для шифрования файла
     */
    @FXML
    private FontAwesomeIconView encrypt;
    /**
     * Кнопка для расшифрования файла
     */
    @FXML
    private FontAwesomeIconView decrypt;
    /**
     * Кнопка для перемещения файла
     */
    @FXML
    private FontAwesomeIconView move;
    /**
     * Кнопка для удаления файла
     */
    @FXML
    private FontAwesomeIconView delete;
    /**
     * Кнопка для копирования файла
     */
    @FXML
    private FontAwesomeIconView copy;
    private FontAwesomeIconView back;
    /**
     * Путь, по которому нужно скопировать/переместить файл
     */
    private static String pathForCopyAndMoving;

    /**
     * Возвращает путь, по которому нужно скопировать/переместить файл
     *
     * @return путь, по которому нужно скопировать/переместить файл
     */
    static String getPathForCopyAndMoving() {
        return pathForCopyAndMoving;
    }

    /**
     * Имя копируемого/перемещаемого файла
     */
    private static String nameForCopyAndMoving;

    /**
     * Возвращает имя копируемого/перемещаемого файла
     *
     * @return имя копируемого/перемещаемого файла
     */
    static String getNameForCopyAndMoving() {
        return nameForCopyAndMoving;
    }

    @FXML
    private void initialize() {
        fillTable(pathField.getText());

        files.setRowFactory(tv -> {
            TableRow<ListFiles> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    File rowData = row.getItem().getFile();
                    if (rowData.isDirectory()) {
                        pathField.setText(rowData.getAbsolutePath());
                    } else {
                        fileField.setText(rowData.getName());
                        pathForCopyAndMoving = rowData.getAbsolutePath();
                        nameForCopyAndMoving = rowData.getName();
                    }
                }
            });
            return row;
        });
        /*
         * Реализация кнопки перехода в новый каталог
         */
        goTo.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            String newPath = pathField.getText();
            fillTable(newPath);
        });


        /*
         * Реализация кнопки удаления файла
         */
        Tooltip onDelete = new Tooltip();
        onDelete.setText("Удалить");
        Tooltip.install(delete,onDelete);
        delete.setOnMouseClicked(event -> {
            File file = new File(pathField.getText() + "\\" + fileField.getText());
            try {
                file.delete();
                openDialogWindow("/view/PaneDeleting.fxml");
                fillTable(pathField.getText());
            } catch (IOException e) {
                e.printStackTrace();
            }
            fillTable(pathField.getText());
        });
        /*
         * Реализация кнопки архивации файла
         */
        Tooltip onArchive = new Tooltip();
        onArchive.setText("Архивировать");
        Tooltip.install(archive,onArchive);
        archive.setOnMouseClicked(event -> {
            String filename = pathField.getText() + "\\" + fileField.getText();
            File file = new File(filename);
            if (filename.substring(filename.lastIndexOf(".") + 1).equals("zip")){
                try {
                    openDialogWindow("/view/PaneAlreadyArchive.fxml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                try (ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(filename + ".zip"));
                     FileInputStream fis = new FileInputStream(filename)) {
                    ZipEntry entry1 = new ZipEntry(fileField.getText());
                    zout.putNextEntry(entry1);
                    // считываем содержимое файла в массив byte
                    byte[] buffer = new byte[fis.available()];
                    fis.read(buffer);
                    // добавляем содержимое к архиву
                    zout.write(buffer);
                    // закрываем текущую запись для новой записи
                    fis.close();
                    zout.closeEntry();
                    zout.close();
                    file.delete();
                    openDialogWindow("/view/PaneArchiving.fxml");
                    fillTable(pathField.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        /*
         * Реализация кнопки празархивации файла
         */
        Tooltip onUnarchive = new Tooltip();
        onUnarchive.setText("Разархивировать");
        Tooltip.install(unarchive,onUnarchive);

        unarchive.setOnMouseClicked(event -> {
            String filename = pathField.getText() + "\\" + fileField.getText();
            File file = new File(filename);
            if (!(filename.substring(filename.lastIndexOf(".") + 1).equals("zip"))){
                try {
                    openDialogWindow("/view/PaneIsNotArchive.fxml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                try (ZipInputStream zin = new ZipInputStream(new FileInputStream(filename))) {
                    ZipEntry entry;
                    String name;
                    while ((entry = zin.getNextEntry()) != null) {
                        name = entry.getName(); // получим название файла
                        // распаковка
                        FileOutputStream fout = new FileOutputStream(pathField.getText() + "\\" + name);
                        for (int c = zin.read(); c != -1; c = zin.read()) {
                            fout.write(c);
                        }
                        fout.flush();
                        fout.close();
                    }
                    zin.closeEntry();
                    zin.close();

                    openDialogWindow("/view/PaneUnarchiving.fxml");
                    file.delete();
                    fillTable(pathField.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        /*
         * Реализация кнопки шифрования файла
         */
        Tooltip onEncryption = new Tooltip();
        onEncryption.setText("Зашифровать");
        Tooltip.install(encrypt,onEncryption);
        encrypt.setOnMouseClicked(event -> {
            String filename = pathField.getText() + "\\" + fileField.getText();
            if (filename.substring(filename.lastIndexOf(".") + 1).equals("csr")){
                try {
                    openDialogWindow("/view/PaneFileAlreadyEncrypt.fxml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                try {
                    FileOutputStream out = new FileOutputStream(filename.replaceAll(fileField.getText(), fileField.getText() + ".csr"));

                    File file = new File(filename);
                    byte[] text = Files.readAllBytes(Paths.get(filename));
                    String key = "dAtAbAsE98765432"; // 128 bit key

                    // Create key and cipher
                    Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
                    Cipher cipher = Cipher.getInstance("AES");

                    cipher.init(Cipher.ENCRYPT_MODE, aesKey);
                    byte[] encrypted = cipher.doFinal(text);

                    for (byte anEncrypted : encrypted) {
                        out.write(anEncrypted);
                    }
                    out.close();
                    file.delete();

                    openDialogWindow("/view/PaneEncryption.fxml");

                    fillTable(pathField.getText());

                } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
        });
        /*
         * Реализация кнопки расшифрования файла
         */
        Tooltip onDecryption = new Tooltip();
        onDecryption.setText("Расшифровать");
        Tooltip.install(decrypt,onDecryption);
        decrypt.setOnMouseClicked(event -> {
            String filename = pathField.getText() + "\\" + fileField.getText();
            File file = new File(filename);
            if (!(filename.substring(filename.lastIndexOf(".") + 1).equals("csr"))) {
                try {
                    openDialogWindow("/view/PaneFileIsNotEncrypt.fxml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                try {
                    FileOutputStream out = new FileOutputStream(filename.replaceAll(".csr", ""));
                    byte[] text = Files.readAllBytes(Paths.get(filename));

                    String key = "dAtAbAsE98765432"; // 128 bit
                    Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
                    Cipher cipher = Cipher.getInstance("AES");

                    cipher.init(Cipher.DECRYPT_MODE, aesKey);
                    byte[] decrypted = cipher.doFinal(text);
                    for (byte anDecrypted : decrypted) {
                        out.write(anDecrypted);
                    }
                    out.close();
                    file.delete();
                    openDialogWindow("/view/PaneDecryption.fxml");
                    fillTable(pathField.getText());

                } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
        });
        /*
         * Реализация кнопки перемещения файла
         */
        Tooltip onMoving = new Tooltip();
        onMoving.setText("Переместить в...");
        Tooltip.install(move,onMoving);
        move.setOnMouseClicked(event -> {
            try {
                openDialogWindow("/view/PaneMoving.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        /*
         * Реализация кнопки копирования
         */
        Tooltip onCopying = new Tooltip();
        onCopying.setText("Скопировать в...");
        Tooltip.install(copy,onCopying);
        copy.setOnMouseClicked(event -> {
            try {
                openDialogWindow("/view/PaneCopy.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Заполняет таблицу по заданному пути
     *
     * @param newPath путь к директории
     */
    private void fillTable(String newPath) {
        files.getItems().clear();
        initData(newPath);
        nameFile.setCellValueFactory(new PropertyValueFactory<>("file"));
        files.setItems(usersData);
    }

    /**
     * Открывает новое диалоговое окно
     *
     * @param pathToPane путь к FXML файлу с конфигурацией окна
     * @throws IOException ошибки
     */
    private void openDialogWindow(String pathToPane) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(pathToPane));
        Stage newWindow = new Stage();
        newWindow.setResizable(false);
        newWindow.initStyle(StageStyle.UTILITY);
        newWindow.setScene(new Scene(root));
        newWindow.show();
    }

    /**
     * Заполнение таблицы содержимым указанного каталога
     *
     * @param path - путь к каталогу
     */
    private void initData(String path) {
        for (File file : Objects.requireNonNull(new File(path).listFiles())) {
            usersData.add(new ListFiles(file));
        }
    }
}