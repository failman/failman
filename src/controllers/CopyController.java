package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Класс-контроллер для копирования файла
 *
 * @authors СмЕшНяФфФкиИ
 */
public class CopyController {
    /**
     * Кнопка для запуска копирования файла
     */
    @FXML
    private Button ok;
    /**
     * Путь, по которому нужно скопировать указанный файл
     */
    @FXML
    private TextField pathTo;

    public void initialize() {
        /*
         * Реализация кнопки запуска копирования файла
         */
        ok.setOnAction(event -> {
            String to = pathTo.getText();
            try {
                Files.copy(Paths.get(MainController.getPathForCopyAndMoving()), Paths.get(to + MainController.getNameForCopyAndMoving()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage stage = (Stage) ok.getScene().getWindow();
            stage.close();
        });
    }
}
